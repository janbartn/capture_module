#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from PySide import QtGui

from image_viewer import Ui_MainWindow

class ImageViewerWindow(QtGui.QMainWindow):
    def __init__(self):
        super(ImageViewerWindow, self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.show()


def main():
    app = QtGui.QApplication(sys.argv)
    ex = ImageViewerWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
