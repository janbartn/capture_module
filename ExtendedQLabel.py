from PySide import QtCore, QtGui

class ExtendedQLabel(QtGui.QLabel):

    def __init__(self, parent, name):
        QtGui.QLabel.__init__(self, parent)
        self.name = name

    def mouseReleaseEvent(self, ev):
        self.emit(QtCore.SIGNAL('clicked()'))

    def __repr__(self):
        return self.name
