# -*- coding: utf-8 -*-
import sys
from PySide import QtCore, QtGui

from design import Ui_MainWindow
from class_timer import framerateTimer

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = framerateTimer()
    myapp.show()
    sys.exit(app.exec_())
