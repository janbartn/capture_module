# -*- coding: utf-8 -*-

from PySide import QtCore, QtGui

import os

from cv2.cv import *
import numpy as np
import cv2
from datetime import datetime, date, time
import ExtendedQLabel
from design import Ui_MainWindow
import subprocess

import image_viewer.image_viewer_window as image_viewer_window

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

class framerateTimer(QtGui.QMainWindow):
    d=datetime.now()
    expath = os.path.expanduser('~')
    path = "/Documents/IronProRecords/"
    ltime =  d.strftime("%A_%d_%B_%Y_%Ih%Mm%p")
    fpath = expath + path + ltime
   
    def __init__(self, parent=None):
        super(framerateTimer, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        #create list of previews
        self.ui.previewList = []
        self.ctimer = QtCore.QTimer()
        # capture declarations
        self.previewImageNumber = 0
        self.previewVideoNumber = 0
        self.previewNumber = 0
        self.camCapture = cv2.VideoCapture(0)
        self.snapshotCounter = 0
        self.recordCounter = 0
        self.recordProcessflag = 0
        self.recordStr = ''
        self.out = ''
        self.videoExt = '.avi'
        self.videoFrameCounter = []
        # startVideo timer
        self.ctimer.start(40)
        QtCore.QObject.connect(self.ctimer, QtCore.SIGNAL("timeout()"), self.startVideoUpdate)
        # buttons
        QtCore.QObject.connect(self.ui.pushButton_2,QtCore.SIGNAL("clicked()"), self.takePicture)
        QtCore.QObject.connect(self.ui.pushButton_3,QtCore.SIGNAL("clicked()"), self.recordVideo)
        self.ui.pushButton.clicked.connect(QtCore.QCoreApplication.instance().quit)
        # Create foulder to store video and images
        if not os.path.exists(self.fpath):
            os.makedirs(self.fpath)   

    def startVideoUpdate(self):
        ret, frame = self.camCapture.read()
        # as opencv mat image is BGR and QPixmap image is RGB we need to convert frame captured
        frameQimage = cv2.cvtColor(frame, cv2.cv.CV_BGR2RGB)
        # converting opencv::mat to pyqt qImage
        qimg = QtGui.QImage(frameQimage.data,frameQimage.shape[1], frameQimage.shape[0], QtGui.QImage.Format_RGB888)
        qpm = QtGui.QPixmap.fromImage(qimg)
        self.ui.label.setPixmap(qpm)
        # create record if record flag is set
        if self.recordProcessflag==1:
            self.out.write(frame)
            self.numCurrentFrame = self.numCurrentFrame + 1

    def takePicture(self):
        self.createNewPreview("f")
        self.snapshotCounter = self.snapshotCounter + 1
        snapshotName = self.fpath + '/Shot'
        snapshotExtension = '.jpg'
        snapshotStr = ''
        ret, frame = self.camCapture.read()
        flashFrame = Scalar(255,255,255)
        snapshotStr = snapshotName+str(self.snapshotCounter)+snapshotExtension 
        cv2.imwrite(snapshotStr,frame)
        print("snapshot!\n %s" % snapshotStr)
        self.pullDownScroller()
        QtCore.QObject.connect(self.ui.pushButton_2, QtCore.SIGNAL("clicked()"), self.ui.textEdit.append(u"Сделана фотография. Записано в файл " + snapshotStr))


    def recordVideo(self):
        recordName = self.fpath + '/Record'
        recordExtension = self.videoExt
        #if flag == 0?
        if self.recordProcessflag==0:
            #assign flag:=1
            self.recordProcessflag = 1
            print("start record!\n")
            self.recordCounter = self.recordCounter + 1
            self.recordStr = recordName + str(self.recordCounter) + recordExtension 
            self.out = cv2.VideoWriter(self.recordStr, cv2.cv.CV_FOURCC('m','p','4','v') , 25.0, (640,480))
            QtCore.QObject.connect(self.ui.pushButton_3, QtCore.SIGNAL("clicked()"), self.ui.textEdit.append(u"Начало видеозаписи в файл " + self.recordStr))
            self.numCurrentFrame = 0
        else :
            #assign flag:=0
            self.recordProcessflag = 0
            #stop record
            print("stop record!\n")
            QtCore.QObject.connect(self.ui.pushButton_3, QtCore.SIGNAL("clicked()"), self.ui.textEdit.append(u"Завершена запись файла " + self.recordStr))
            self.videoFrameCounter.append(self.numCurrentFrame)
            print("recorded file containes ", self.numCurrentFrame, " frames")
            self.pullDownScroller()
            self.createNewPreview("v")
            self.out.release()




    def createNewPreview(self, fileType):
        self.previewNumber = self.previewNumber + 1
        if fileType == 'f':
            self.previewImageNumber = self.previewImageNumber + 1
            fileType = fileType + str(self.previewImageNumber)
        else :
            self.previewVideoNumber = self.previewVideoNumber + 1
            fileType = fileType + str(self.previewVideoNumber)
        previewName = "preview" + str(self.previewNumber)
        label = ExtendedQLabel.ExtendedQLabel(self, fileType)
        label.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHeightForWidth(label.sizePolicy().hasHeightForWidth())
        label.setSizePolicy(sizePolicy)
        label.setMaximumSize(QtCore.QSize(260, 220))
        label.setFrameShape(QtGui.QFrame.Box)
        label.setText(_fromUtf8(""))
        label.setObjectName(_fromUtf8(previewName))
        label.setScaledContents(True);
        self.ui.verticalLayout_5.addWidget(label)
        self.ui.previewList.append(label)
        # get current frame to show in preview
        ret, frame = self.camCapture.read()
        # as opencv mat image is BGR and QPixmap image is RGB we need to convert frame captured
        frame = cv2.cvtColor(frame, cv2.cv.CV_BGR2RGB)
        # converting opencv::mat to pyqt qImage
        qimg = QtGui.QImage(frame.data,frame.shape[1], frame.shape[0], QtGui.QImage.Format_RGB888)
        qpm = QtGui.QPixmap.fromImage(qimg)
        self.ui.previewList[self.previewNumber - 1].setPixmap(qpm)
        self.connect(self.ui.previewList[self.previewNumber - 1], QtCore.SIGNAL('clicked()'), self.labelClicked)


    def pullDownScroller(self):
        # scroll down scroll bar to view latest preview
        self.ui.scrollArea.verticalScrollBar().setSliderPosition(9999999)
        self.ui.textEdit.verticalScrollBar().setSliderPosition(9999999)

    def labelClicked(self):
        fileType = str(self.sender())
        if fileType[0] == 'f':
            pathToPic = str(self.fpath + '/Shot' + str(fileType[1]) + '.jpg')
            self.ImageViewer_window = image_viewer_window.ImageViewerWindow()
            self.ImageViewer_window.ui.label.setPixmap(QtGui.QPixmap(pathToPic))
        else :
            pathToMov = str(self.fpath + '/Record' + str(fileType[1]) + self.videoExt)
            subprocess.Popen(['vlc', pathToMov])



